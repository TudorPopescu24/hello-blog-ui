using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace hello_blog_ui.Entities
{
    public class LoginApiResponse
    {
        public IEnumerable<Cookie> Cookies { get; set; }
        public bool IsSuccessStatusCode { get; set; }
        public HttpContent Content { get; set; }
    }
}