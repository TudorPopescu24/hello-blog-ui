namespace hello_blog_ui.Entities
{
    public class AccountCredentials
    {
        public AccountCredentials(string email, string password)
        {
            Email = email;
            Password = password;
        }
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
