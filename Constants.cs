namespace hello_blog_ui
{
    public static class Constants
    {
        public static class Api
        {
            private const string BaseUrl = "http://localhost:5000/";
            public const string BlogPost = BaseUrl + "api/v1/blogpost";
            public const string BlogPostViews = BaseUrl + "api/v1/blogPostViews";
            public const string BlogViews = BaseUrl + "api/v1/blogViews";
            public const string Register = BaseUrl + "api/v1/account/register";
            public const string Login = BaseUrl + "api/v1/account/login";
            public const string Logout = BaseUrl + "api/v1/account/logout";
            public const string AdministrationRole = BaseUrl + "api/v1/administration/role";
        }
    }
}
