using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace hello_blog_ui.Controllers
{
    ///<summary>
    /// Was added to not have duplicate code that can be in the controllers
    ///</summary>
    public class BaseController : Controller
    {
        protected string GetCookie()
        {
            return ".AspNetCore.Identity.Application=" + HttpContext.Request.Cookies[".AspNetCore.Identity.Application"];
        }

        protected void SetSessionData(string role)
        {
            HttpContext.Session.SetString("isAuthenticated", "true");
            HttpContext.Session.SetString("role", role);
        }

        protected void ClearSessionData()
        {
            Response.Cookies.Delete(".AspNetCore.Identity.Application");
            HttpContext.Session.Clear();
        }

        protected void SetUIResponseCookies(IEnumerable<Cookie> cookies)
        {
            foreach (var cookie in cookies)
            {
                Response.Cookies.Append(cookie.Name, cookie.Value);
            }
        }
        
        protected bool CheckAuthentication()
        {
            var isAuthenticated = HttpContext.Session.GetString("isAuthenticated");
            if (isAuthenticated != null)
                return Convert.ToBoolean(isAuthenticated);
            return false;
        }
    }
}