using System;
using System.Diagnostics;
using System.Threading.Tasks;
using hello_blog_ui.Models;
using Microsoft.AspNetCore.Mvc;
using hello_blog_ui.Managers;
using hello_blog_ui.Interfaces;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace hello_blog_ui.Controllers
{
    ///<summary>
    /// The only responsibility of BlogPostController class is to receive UI requests
    ///     - pass what is need to other layers
    ///     - respond to UI
    /// Version of the class obtained applying 
    ///     - SRP (create a new class that is responsible to build images)
    ///     - OCP (We've add the BulkPublish method (Open for extension))
    ///     - DIP (We've use Dependency Injection (DI) to inject what we need and create abstractions and get instance at Run Time)
    ///         - Regarding memory issue we should not instantiate classes and let .NET to do that.
    ///             - Just register the service in the Startup class -> RegisterServices method
    ///         - Controller pass the information to managers
    ///         - Controller does not know what is the implementation of managers
    ///</summary>
    public class BlogPostController : BaseController
    {
        private readonly IBlogPostApiManager _blogPostApiManager;
        private readonly IBlogPostManager _blogPostManager;
        private readonly IBlogViewsApiManager _blogViewsApiManager;
        private readonly IBlogPostViewsApiManager _blogPostViewsApiManager;

        public BlogPostController(IBlogPostApiManager blogPostApiManager,
                                  IBlogPostManager blogPostManager,
                                  IBlogViewsApiManager blogViewsApiManager,
                                  IBlogPostViewsApiManager blogPostViewsApiManager)
        {
            _blogPostApiManager = blogPostApiManager;
            _blogPostManager = blogPostManager;
            _blogViewsApiManager = blogViewsApiManager;
            _blogPostViewsApiManager = blogPostViewsApiManager;
        }

        ///<summary>
        /// Was refactored to pass the responsibility of building images to other layers
        ///</summary>
        public async Task<IActionResult> Index()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var blogPosts = Task.Run(() => _blogPostManager.GetBlogPosts());
            var viewsResponse = Task.Run(() => _blogViewsApiManager.GetAsync());
            await Task.WhenAll(blogPosts, viewsResponse);

            stopWatch.Stop();
            TimeSpan executionTime = TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds);
            var views = viewsResponse.Result.Content.ReadAsStringAsync().Result;
            var model = new BlogPostModel
            {
                BlogPosts = blogPosts?.GetAwaiter().GetResult(),
                ExecutionTime = executionTime.ToString(@"ss\:fff"),
                BlogNumberOfViews = JsonConvert.DeserializeObject<BlogViewsModel>(views)?.Views.ToString(),
                IsAuthenticated = CheckAuthentication()
            };

            if (!CheckAuthentication())
                Task.Factory.StartNew(() => { _blogViewsApiManager.UpdateAsync(); });

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        ///<summary>
        /// Was refactored to pass the responsibility of save blog post other layers
        ///</summary>
        [HttpPost]
        public async Task<IActionResult> Create(BlogPostApiModel blogPost)
        {
            if (ModelState.IsValid)
            {
                var cookieValue = GetCookie();
                await _blogPostApiManager.CreateAsync(blogPost, cookieValue);
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View();
            }
        }

        public async Task<IActionResult> View(int blogPostId)
        {
            var blogPost = _blogPostManager.GetBlogPostById(blogPostId);

            if (blogPost.Id < 1)
            {
                return View("Error", new ErrorModel { Error = "The blog post does not exist" });
            }

            // update number of views only if this a guest
            if(!CheckAuthentication())
                Task.Run(() => _blogPostViewsApiManager.UpdateAsync(blogPostId));

            return View(blogPost?.Result);
        }

        public async Task<IActionResult> Edit(int blogPostId, bool edit = false)
        {
            if (CheckAuthentication())
            {
                var model = new BlogPostApiModel();

                var blogPost = Task.Run(() => _blogPostManager.GetBlogPostById(blogPostId));
                var blogPostNumberOfViewes = Task.Run(() => _blogPostViewsApiManager.GetAsync(blogPostId));
                await Task.WhenAll(blogPost, blogPostNumberOfViewes);

                var blogPostViews = blogPostNumberOfViewes.Result.Content.ReadAsStringAsync().Result;
                var numberOfViews = JsonConvert.DeserializeObject<BlogPostViewsModel>(blogPostViews)?.Views.ToString();
                if (blogPost?.Result != null)
                {
                    blogPost.Result.Edit = edit;
                    blogPost.Result.NumberOfViews = !string.IsNullOrWhiteSpace(numberOfViews) ? numberOfViews : "0";
                }

                if (blogPost?.Result?.Id < 1)
                {
                    return View("Error", new ErrorModel { Error = "The blog post does not exist" });
                }

                return View(blogPost?.Result);
            }
            return View("Error", new ErrorModel { Error = "Operation not allowed! User is log authenticated." });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(BlogPostApiModel blogPost)
        {
            if (CheckAuthentication())
            {
                var cookieValue = GetCookie();
                var updated = await _blogPostManager.Update(blogPost, cookieValue);

                if (!updated)
                {
                    return View("Error", new ErrorModel { Error = "The blog post does not exist" });
                }
                return RedirectToAction(nameof(Index));
            }
            return View("Error", new ErrorModel { Error = "Operation not allowed! User is log authenticated." });
        }

        public async Task<IActionResult> Delete(int blogPostId)
        {
            if (CheckAuthentication())
            {
                var cookieValue = GetCookie();
                var deleted = await _blogPostManager.DeleteBlogPostById(blogPostId, cookieValue);

                if (!deleted)
                {
                    return View("Error", new ErrorModel { Error = "The blog post does not exist" });
                }
                return RedirectToAction(nameof(Index));
            }
            return View("Error", new ErrorModel { Error = "Operation not allowed! User is log authenticated." });
        }

        ///<summary>
        /// Was added to show OCP (Open for extension)
        ///</summary>
        public async Task<IActionResult> BulkPublish([FromServices] IServiceProvider serviceProvider, BlogPostModel blogPostModel)
        {
            var formFile = blogPostModel.BulkBlogPosts;
            if (formFile?.Length > 0)
            {
                var fileName = formFile.FileName;
                var fileTypeString = Path.GetExtension(fileName).Substring(1);
                switch (fileTypeString)
                {
                    case "xml":
                        var serviceInstance = serviceProvider.GetService<ProcessXmlBlogData>();
                        await serviceInstance.PublishFromFile(formFile);
                        break;

                    case "json":
                        var serviceInstance1 = serviceProvider.GetService<ProcessJsonBlogData>();
                        await serviceInstance1.PublishFromFile(formFile);
                        break;
                    default:
                        break;
                }
            }

            return Redirect(nameof(Index));
        }
    }
}