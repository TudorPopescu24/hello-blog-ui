using System.Threading.Tasks;
using hello_blog_ui.Entities;
using hello_blog_ui.Interfaces;
using hello_blog_ui.Models;
using Microsoft.AspNetCore.Mvc;

namespace hello_blog_ui.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountApiManager _accountApiManager;
        public AccountController(IAccountApiManager accountApiManager)
        {
            _accountApiManager = accountApiManager;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(AccountCredentialsModel credentials, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var accountCredentials = new AccountCredentials(credentials.Email, credentials.Password);
            var loginRespose = await _accountApiManager.Login(accountCredentials);

            if (loginRespose.IsSuccessStatusCode)
            {
                SetUIResponseCookies(loginRespose.Cookies);
                var role = await loginRespose.Content.ReadAsStringAsync();
                SetSessionData(role);
            }
            else
            {
                ViewBag.Error = await loginRespose.Content.ReadAsStringAsync();
                return View();
            }

            if (!string.IsNullOrEmpty(returnUrl))
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "BlogPost");
            }
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(UserModel user)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var accountCredentials = new AccountCredentials(user.Email, user.Password);
            var result = await _accountApiManager.Register(accountCredentials);

            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction(nameof(Login));
            }
            else
            {
                ViewBag.Error = result.Content.ReadAsStringAsync().Result;
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            var response = await _accountApiManager.Logout();
            ClearSessionData();

            return RedirectToAction("Index", "BlogPost");
        }
    }
}