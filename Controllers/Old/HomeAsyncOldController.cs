using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace hello_blog_ui.Controllers
{
    public class HomeAsyncOldController : Controller
    {
        private const string requestUri = "http://localhost:5000/api/v1/blogpost";
        private const string noOfViewsUri = "http://localhost:5000/api/v1/BlogPostViews";


        public async Task<IActionResult> Index()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            List<BlogPostApiModel> blogPosts = new List<BlogPostApiModel>();
            var httpClient = new HttpClient();

            var response = Task.Run(() => httpClient.GetAsync(requestUri));
            var images = Task.Run(() => BuildImageList());

            // Task.WaitAll(response, images);
            await Task.WhenAll(response, images);

            string apiResponse = response.Result.Content.ReadAsStringAsync().Result;
            blogPosts = JsonConvert.DeserializeObject<List<BlogPostApiModel>>(apiResponse);
            // encoding the html
            // foreach(var blogPost in blogPosts)
            // {
            //     blogPost.Content = htmlEncoder.Encode(blogPost.Content);
            // }

            var random = new Random();

            foreach (var blogPost in blogPosts)
            {
                var rnd = random.Next(images.Result.Count);
                blogPost.ImageUrl = images.Result[rnd];
            }
            stopWatch.Stop();
            TimeSpan executioTime = TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds);
            var model = new BlogPostModel
            {
                BlogPosts = blogPosts,
                ExecutionTime = executioTime.ToString(@"ss\:fff")
            };


            Task.Factory.StartNew(() => { UpdateNumberOfViews(); });

            return View(model);
        }

        private void UpdateNumberOfViews()
        {
            var httpClient = new HttpClient();
            httpClient.PutAsync(noOfViewsUri, null);
        }

        private async Task<List<string>> BuildImageList()
        {
            var onlineImages = new List<string>();
            // blocheaza Thread-ul pentru o anumita perioada de timp.
            // Daca este nevoie ar trebui utilizat Tas.Delay
            // Thread.Sleep(3000);
            await Task.Delay(3000);
            onlineImages.Add("https://i.pinimg.com/236x/aa/e2/26/aae2269344e98521aa0e624323f4c549.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/ea/3d/cf/ea3dcff8ed8e8939d98c96b81f747623--happy-faces-smiley-faces.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/5a/65/d3/5a65d339c8745ad479a97a89e910dfb7.jpg");
            onlineImages.Add("https://thenational-the-national-prod.cdn.arcpublishing.com/resizer/DSrQTaVkKg6mroH2A5aVqacrRv0=/1600x0/filters:format(jpg):quality(70)/cloudfront-eu-central-1.images.arcpublishing.com/thenational/VBNNUCG3YUTHC3WQNEW5UXQ2GQ.jpg");
            onlineImages.Add("https://cdn.pixabay.com/photo/2019/02/19/19/45/thumbs-up-4007573_960_720.png");
            onlineImages.Add("https://i.pinimg.com/236x/cd/6e/60/cd6e60e9d2406ff98cecca53f9d77899.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/5a/5c/24/5a5c2488064a07a077d3d5d6e3a496aa--symbols-emoticons-emoji-emoticons.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/9b/40/27/9b402780321d2e3fb212681f41387857--smiley-faces-emojis.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/20/11/8c/20118ca18072c3345cee77f2d4589bad--smiley-facebook-symbols-emoticons.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/7f/b2/6d/7fb26dd5190f796de38c948f1eb8cb8d--smiley-emoji-emoji-faces.jpg");
            onlineImages.Add("https://i.pinimg.com/236x/f3/9f/9a/f39f9a92122617322d34aba7d54e53b0--emoji--emoji-faces.jpg");
            onlineImages.Add("https://m.economictimes.com/thumb/height-450,width-600,imgsize-41343,msid-56993687/the-worlds-oldest-emoji-dates-back-to-1635.jpg");
            onlineImages.Add("https://townsquare.media/site/694/files/2019/01/GettyImages-868643608.jpg");
            onlineImages.Add("https://happybb.ro/media/catalog/product/cache/e04654d74fc7a4e86d5e50cb4755c5fb/b/a/balon-folie-orbz-sfera-smiley-face-43-cm-northstar-balloons-01135_hfwtkaqpjfgzcrev.jpg");

            return onlineImages;
        }

    }
}