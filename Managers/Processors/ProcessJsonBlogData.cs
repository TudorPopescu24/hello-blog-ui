using System.Threading.Tasks;
using hello_blog_ui.Interfaces;
using Microsoft.AspNetCore.Http;

namespace hello_blog_ui.Managers
{
    ///<summary>
    /// Class created to respect OCP. It does not have real implementation, just to demonstrated the principle
    ///</summary>
    public class ProcessJsonBlogData : ProcessBlogDataBase, IProcessBlogData
    {
        public Task<bool> PublishFromFile(IFormFile formFile)
        {
            // parse Json file to DTO object
            // publish tp json service
            return Task.FromResult(true);
        }
    }
}