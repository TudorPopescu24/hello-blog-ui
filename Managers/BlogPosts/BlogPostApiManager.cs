using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;
using Newtonsoft.Json;
using System.Text;
using hello_blog_ui.Models;

namespace hello_blog_ui
{
    ///<summary> 
    /// Responsibility : to make api calls and return response
    /// Class obtained applying 
    ///     - ISP (we've add abstractions using interfaces ( First was added IManager interface, 
    ///            after following ICP we've split the big interface into smaller ones )
    ///     - DIP (We inject IHttpClientManager manager to do the actual Http calls)
    ///         - Regarding memory issue we should not instantiate classes and let .NET to do that.
    ///             - Just register the service in the Startup class -> RegisterServices method
    ///</summary>
    public class BlogPostApiManager : IBlogPostApiManager
    {
        private readonly IHttlpClientManager _httlpClientManager;

        public BlogPostApiManager(IHttlpClientManager httlpClientManager)
        {
            _httlpClientManager = httlpClientManager;
        }

        public Task<HttpResponseMessage> GetBlogPostByIdAsync(int blogPostId)
        {
            return _httlpClientManager.GetAsync(Constants.Api.BlogPost + "/" + blogPostId);
        }

        public Task<HttpResponseMessage> GetAllAsync()
        {
            return _httlpClientManager.GetAsync(Constants.Api.BlogPost);
        }

        public async Task<HttpResponseMessage> CreateAsync(BlogPostApiModel blogPost, string cookieValue)
        {
            var payload = JsonConvert.SerializeObject(blogPost);
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
            return await _httlpClientManager.PostAsync(Constants.Api.BlogPost, content, cookieValue);
        }

        public async Task<HttpResponseMessage> UpdateAsync(BlogPostApiModel blogPost, string cookieValue)
        {
            var payload = JsonConvert.SerializeObject(blogPost);
            HttpContent content = new StringContent(payload, Encoding.UTF8, "application/json");
            return await _httlpClientManager.PutAsync(Constants.Api.BlogPost, content, cookieValue);
        }

        public async Task<HttpResponseMessage> DeleteAsync(int blogPostId, string cookieValue)
        {
            return await _httlpClientManager.DeleteAsync(Constants.Api.BlogPost + "/" + blogPostId, cookieValue);
        }
    }
}