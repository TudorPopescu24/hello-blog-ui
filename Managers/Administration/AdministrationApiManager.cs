using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Interfaces;

namespace hello_blog_ui.Managers
{
    public class AdministrationApiManager : IAdministrationApiManager
    {
        private readonly IHttlpClientManager _httlpClientManager;
        public AdministrationApiManager(IHttlpClientManager httlpClientManager)
        {
            _httlpClientManager = httlpClientManager;
        }
        
        public async Task<HttpResponseMessage> GetRolesAsync(string cookieValue)
        {
            return await _httlpClientManager.GetAsync(Constants.Api.AdministrationRole, cookieValue);
        }
    }
}