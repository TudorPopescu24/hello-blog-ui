using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using hello_blog_ui.Entities;
using hello_blog_ui.Interfaces;

namespace hello_blog_ui.Managers
{
    ///<summary> 
    /// Responsibility : to instantiate a new instance of HTTP Client and make specific calls
    /// Class obtained applying 
    ///     - SRP (create a new class that is responsible to initiate http client calls)
    ///</summary>
    public class HttlpClientManager : IHttlpClientManager
    {
        private readonly HttpClient _httpClient;

        public HttlpClientManager()
        {
            _httpClient = new HttpClient();
        }

        public Task<HttpResponseMessage> GetAsync(string url, string cookieValue = null)
        {
            if (cookieValue != null)
            {
                _httpClient.DefaultRequestHeaders.Add("Cookie", cookieValue);
                _httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            }
            return _httpClient.GetAsync(url);
        }

        public async Task<HttpResponseMessage> PostAsync(string url, HttpContent content, string cookieValue)
        {
            if (cookieValue != null)
                _httpClient.DefaultRequestHeaders.Add("Cookie", cookieValue);

            _httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            return await _httpClient.PostAsync(url, content);
        }

        public async Task<LoginApiResponse> PostWithHandlerAsync(Uri uri, HttpContent content)
        {
            var cookies = new CookieContainer();
            using (var handler = new HttpClientHandler { UseCookies = true, CookieContainer = cookies })
            using (var httpClient = new HttpClient(handler))
            {
                var response = await httpClient.PostAsync(uri, content);
                return new LoginApiResponse
                {
                    IsSuccessStatusCode = response.IsSuccessStatusCode,
                    Cookies = cookies.GetCookies(uri).Cast<Cookie>(),
                    Content = response.Content
                };
            }
        }

        public Task<HttpResponseMessage> PutAsync(string url, HttpContent content)
        {
            return _httpClient.PutAsync(url, content);
        }

        public async Task<HttpResponseMessage> PutAsync(string url, HttpContent content, string cookieValue)
        {
            if (cookieValue != null)
                _httpClient.DefaultRequestHeaders.Add("Cookie", cookieValue);

            _httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            return await _httpClient.PutAsync(url, content);
        }

        public async Task<HttpResponseMessage> DeleteAsync(string url, string cookieValue)
        {
            if (cookieValue != null)
                _httpClient.DefaultRequestHeaders.Add("Cookie", cookieValue);

            _httpClient.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            return await _httpClient.DeleteAsync(url);
        }
    }
}