using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace hello_blog_ui.Interfaces
{
    public interface IProcessBlogData
    {
        Task<bool> PublishFromFile(IFormFile formFile);
    }
}