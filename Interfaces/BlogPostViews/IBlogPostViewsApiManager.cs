using System.Net.Http;
using System.Threading.Tasks;

namespace hello_blog_ui.Interfaces
{
    public interface IBlogPostViewsApiManager
    {
        Task<HttpResponseMessage> GetAsync(int blogPostId);
        Task<HttpResponseMessage> UpdateAsync(int blogPostId);
    }
}