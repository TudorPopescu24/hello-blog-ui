using System.Collections.Generic;
using System.Threading.Tasks;
using hello_blog_ui.Models;

namespace hello_blog_ui.Interfaces
{
    public interface IBlogPostManager
    {
        Task<List<BlogPostApiModel>> GetBlogPosts();
        Task<BlogPostApiModel> GetBlogPostById(int blogPostId);
        Task<bool> DeleteBlogPostById(int blogPostId, string cookieValue);
        Task<bool> Update(BlogPostApiModel blogPost, string cookieValue);
    }
}