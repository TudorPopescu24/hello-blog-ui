using System.Net.Http;
using System.Threading.Tasks;

namespace hello_blog_ui.Interfaces
{
    public interface IBlogViewsApiManager
    {
        Task<HttpResponseMessage> GetAsync();
        Task<HttpResponseMessage> UpdateAsync();
    }
}