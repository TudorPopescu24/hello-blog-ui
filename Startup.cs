using System;
using hello_blog_ui.Interfaces;
using hello_blog_ui.Managers;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace hello_blog_ui
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(24);//You can set Time   
            });
            services.AddControllersWithViews();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            RegisterServices(services);
        }

        ///<summary>
        ///Register all the services (interfaces) that are need it in the application
        ///Let the framework create the instances
        ///</summary>
        private void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IImageManager, ImageManager>();
            services.AddScoped<IBlogPostApiManager, BlogPostApiManager>();
            services.AddScoped<IBlogPostManager, BlogPostManager>();
            services.AddScoped<IAccountApiManager, AccountApiManager>();
            services.AddScoped<IAdministrationApiManager, AdministrationApiManager>();
            services.AddScoped<IBlogPostViewsApiManager, BlogPostViewsApiManager>();
            services.AddScoped<IBlogViewsApiManager, BlogViewsApiManager>();
            services.AddScoped<IHttlpClientManager, HttlpClientManager>();

            // services.AddScoped<IProcessBlogData, ProcessXmlBlogData>();
            // services.AddScoped<IProcessBlogData, ProcessJsonBlogData>();

            services.AddScoped<ProcessXmlBlogData>();
            services.AddScoped<ProcessJsonBlogData>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=BlogPost}/{action=Index}/{id?}"); ;
            });
        }
    }
}
